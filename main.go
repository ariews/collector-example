package main

import (
	"fmt"
	"os"
	"sort"

	"github.com/olekukonko/tablewriter"
	"gitlab.com/ariews/collector"
)

func sortItems(items []collector.SiteData) []collector.SiteData {
	sort.SliceStable(items, func(i, j int) bool {
		return items[i].Identifier < items[j].Identifier
	})

	return items
}

func main() {
	c := collector.New()

	// register Joke API
	c.AddSite(&JokeAPI{})
	c.AddSite(&GitlabAPI{})

	// add transformer
	c.AddTransformer(sortItems)

	fmt.Println("Feed")
	printWithTable(c.Feed())

	fmt.Println("Search")
	printWithTable(c.Search("Work"))

	fmt.Println("Get detail")
	item := c.Detail("ja:12", false)
	renderTable([][]string{{item.Identifier, item.Title}})

	item = c.Detail("gl:30914896", false)
	renderTable([][]string{{item.Identifier, item.Title}})
}

func printWithTable(data []collector.SiteData) {
	var items [][]string
	for i := range data {
		items = append(items, []string{data[i].Identifier, data[i].Title})
	}
	renderTable(items)
}

func renderTable(data [][]string) {
	table := tablewriter.NewWriter(os.Stdout)
	table.SetHeader([]string{"Identifier", "Title"})
	table.SetRowLine(true)
	table.SetRowSeparator("-")
	table.AppendBulk(data)
	table.Render()
}
