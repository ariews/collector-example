package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"gitlab.com/ariews/collector"
)

type GitlabAPI struct{}

func (g *GitlabAPI) Name() string {
	return "Gitlab API"
}

func (g *GitlabAPI) ID() string {
	return "gl"
}

func (g *GitlabAPI) Feed() collector.SiteSource {
	return collector.SiteSource{
		URL:         "https://gitlab.com/api/v4/projects",
		ProcessByte: g.postProcessor,
		PageCallback: func(i, j int) (int, int) {
			log.Println("call the callback", i, j)
			return i, j
		},
	}
}

func (g *GitlabAPI) Search() collector.SiteSource {
	return collector.SiteSource{
		URL:         "https://gitlab.com/api/v4/projects?search=%v",
		ProcessByte: g.postProcessor,
	}
}

func (g *GitlabAPI) Detail() collector.SiteSource {
	return collector.SiteSource{
		URL:         "https://gitlab.com/api/v4/projects/%v",
		ProcessByte: g.detailProcessor,
	}
}

func (g *GitlabAPI) HttpClient() *http.Client {
	return collector.DefaultHttpClient()
}

type gitlabProject struct {
	ID   int    `json:"id"`
	Name string `json:"name_with_namespace"`
}

type gitlabFeed []gitlabProject

func (g *GitlabAPI) postProcessor(b []byte, csd chan collector.SiteData, cer chan error) {
	var feed gitlabFeed

	err := json.Unmarshal(b, &feed)
	if err != nil {
		cer <- fmt.Errorf("failed unmarshal: %v", err)
		return
	}

	for i := range feed {
		g.sproj(feed[i], csd)
	}
}

func (g *GitlabAPI) detailProcessor(b []byte, csd chan collector.SiteData, cer chan error) {
	var proj gitlabProject

	err := json.Unmarshal(b, &proj)
	if err != nil {
		cer <- fmt.Errorf("failed unmarshal: %v", err)
		return
	}

	g.sproj(proj, csd)
}

func (g *GitlabAPI) sproj(p gitlabProject, csd chan collector.SiteData) {
	csd <- collector.SiteData{
		Title:      p.Name,
		Identifier: collector.MakeID(g.ID(), p.ID),
	}
}
