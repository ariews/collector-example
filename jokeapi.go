package main

import (
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/ariews/collector"
)

// Joke API - https://sv443.net/jokeapi/v2/

type JokeAPIFeed struct {
	Error   bool   `json:"error"`
	Amount  int    `json:"amount"`
	Message string `json:"message"`
	Jokes   []struct {
		Category string `json:"category"`
		Type     string `json:"type"`
		Joke     string `json:"joke,omitempty"`
		ID       int    `json:"id"`
	} `json:"jokes"`
}

type JokeAPI struct {
}

func (j *JokeAPI) Name() string {
	return "Joke API"
}

func (j *JokeAPI) ID() string {
	return "ja"
}

func (j *JokeAPI) Feed() collector.SiteSource {
	return collector.SiteSource{
		URL:         "https://v2.jokeapi.dev/joke/Programming?type=single&amount=10",
		ProcessByte: j.postProcessor,
	}
}

func (j *JokeAPI) Search() collector.SiteSource {
	return collector.SiteSource{
		URL:         "https://v2.jokeapi.dev/joke/Programming?type=single&contains=%v&amount=10",
		ProcessByte: j.postProcessor,
	}
}

func (j *JokeAPI) Detail() collector.SiteSource {
	return collector.SiteSource{
		URL:         "https://v2.jokeapi.dev/joke/Programming?type=single&idRange=%v&amount=10",
		ProcessByte: j.postProcessor,
	}
}

func (j *JokeAPI) HttpClient() *http.Client {
	return collector.DefaultHttpClient()
}

func (j *JokeAPI) postProcessor(b []byte, csd chan collector.SiteData, cer chan error) {
	var jaf JokeAPIFeed

	err := json.Unmarshal(b, &jaf)
	if err != nil {
		cer <- fmt.Errorf("failed unmarshal: %v", err)
		return
	}

	if jaf.Error {
		cer <- fmt.Errorf("failed unmarshal: %v", jaf.Message)
		return
	}

	for i := range jaf.Jokes {
		csd <- collector.SiteData{
			Title:      jaf.Jokes[i].Joke,
			Identifier: collector.MakeID(j.ID(), jaf.Jokes[i].ID),
		}
	}
}
