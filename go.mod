module gitlab.com/ariews/collector-example

go 1.16

require (
	github.com/olekukonko/tablewriter v0.0.5
	gitlab.com/ariews/collector v1.1.3
)
